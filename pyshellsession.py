import asyncio
import subprocess
import sys
import json

class shell_exception(BaseException):
    pass
class shell_syntax_exception(shell_exception):
    pass
class pyshellsession:
    def __init__(self,default_shell_program=None):
        self.default_shell_program=default_shell_program
    #@classmethod
    #def open_new_session(cls):
    #    return cls()
    #default_session = open_new_session
    async def get_output(self,popen_subprocess):
        def still_alive():
            return popen_subprocess.poll() is None
        while still_alive():
            await asyncio.sleep(1)
            #print("Still alive, waiting for finished...")
        output,err = popen_subprocess.communicate()
        output = output.decode().strip().replace("\r\n"," ")
        if err:
            err = err.decode().strip().replace("\r\n"," ")
            if "is not recognized as an internal or external command, operable program or batch file." in err:
                raise shell_syntax_exception(err)
            else:
                raise shell_exception(err)
        if not output:
            output = None
        return output
    def __call__(self,command: str,shell_program=None):
        if shell_program is not None:
            command = [shell_program,command]
        elif self.default_shell_program is not None:
            command = [self.default_shell_program,command]
        p = subprocess.Popen(command,stdout=subprocess.PIPE,stderr=subprocess.PIPE,shell=True)
        loop = asyncio.get_event_loop()
        output = loop.run_until_complete(self.get_output(p))
        loop.close()
        print("output: ",output)
        
class pyshellobject:
    def __init__(self,json_str: str):
        self.original_json_str = json_str
        try:
            self.json_dict = json.loads(json_str)
        except json.decoder.JSONDecodeError as e:
            raise ValueError(str(e)+"\nInvalid json string: "+json_str).with_traceback(sys.exc_info()[2])
    def __getitem__(self,key):
        return self.json_dict[key]
    def __setitem__(self,key,value):
        self.json_dict[key] = value
    def as_json(self) -> str:
        return json.dumps(self.json_dict,separators=(',',':'))
pss = pyshellsession(default_shell_program="powershell.exe")
#pss("echo hi")
pss("read-host gimme stuff")
